# voxl-hal3-tof-cam-ros

Hal3 Tof app that gets RAW TOF data from the camera module. The app then calls the PMD libs to do the post-processing of the RAW data. The post-processed data like IR Image, Depth Image etc is published to ROS topics that can be viewed using the RViz tool.

## Build and install the project using `voxl-emulator`:

- Clone project:

```bash
git clone https://gitlab.com/voxl-public/voxl-hal3-tof-cam-ros.git
cd voxl-hal3-tof-cam-ros
```

- Run voxl-emulator docker
  - Note: information regarding the voxl-emulator can be found [here](https://gitlab.com/voxl-public/voxl-docker)

```bash
voxl-docker -i voxl-emulator
```

- Build project binary:

```bash
source /opt/ros/indigo/setup.bash
./build.sh source/
```

This will generate a binary in "devel/lib/voxl_hal3_tof_cam_ros/voxl_hal3_tof_cam_ros_node"

- Build the IPK

```bash
./make_package.sh
```

- If VOXL is connected via adb, you can install on  target;

```bash
./install_on_voxl.sh
```

## Build and install the project directly on `VOXL`:
```
bash
mkdir -p /home/root/git
cd /home/root/git
git clone https://gitlab.com/voxl-public/voxl-hal3-tof-cam-ros.git
cd voxl-hal3-tof-cam-ros
./build.sh
./make_package.sh
opkg install ./voxl-hal3-tof-cam-ros_0.0.3.ipk
```

## Run TOF Application on VOXL

### Determine VOXL IP Address
- For WiFi setup, instructions found [here](https://docs.modalai.com/wifi-setup/)
- Run `ifconfig` in a terminal
- Check `inet addr` value for `wlan0` (if using wifi), this will give you the IP address of VOXL
- `hostname -i` also works

### Start TOF ROS Node
- If needed, open a new terminal on VOXL (use adb or ssh)
- Make sure your terminal is using bash (when in doubt just run `bash` after opening the terminal)
- Identify the correct camera ID to use for TOF sensor
   - For camera id please check [here](https://docs.modalai.com/camera-connections/#configurations)
- Export TOF_CAM_ID in your environment before running the TOF application, for example
   - `export TOF_CAM_ID=1`
      - starting with release 0.0.3, you can choose to autodetect TOF camera id
      - `export TOF_CAM_ID=-1` will tell the application to attempt to autodetect TOF camera id
- By default all outputs are enabled i.e. IR-Image, Depth-Image, Point-Cloud (see `tof.launch`)

- When running package installed to `/opt/ros/indigo` use:

#### Start Installed TOF ROS Node
```
bash
export ROS_IP=`hostname -i`
export TOF_CAM_ID=-1
source /opt/ros/indigo/setup.bash
roslaunch /opt/ros/indigo/share/voxl_hal3_tof_cam_ros/launch/tof.launch
```

- When running a custom build of the package use (assuming it's in `/home/root/git/voxl-hal3-tof-cam-ros`):
#### Start Locally Built TOF ROS Node
```
# this will re-build the code and launch the app
bash
export ROS_IP=`hostname -i`
export TOF_CAM_ID=-1
cd /home/root/git/voxl-hal3-tof-cam-ros
./clean.sh
./build.sh
source ./devel/setup.bash
roslaunch ./source/launch/tof.launch
```

### Expected Behavior
- the very first time the sensor is used, it will be initialized (about 30 seconds)
- lens parameters will be downloaded from sensor if needed to `/data/misc/camera/irs10x0c_lens.cal`
```
yocto:~/git/voxl-hal3-tof-cam-ros# roslaunch ./source/launch/tof.launch  
... logging to /home/root/.ros/log/bad5037a-561d-11eb-a4e9-90cdb698b2f1/roslaunch-apq8096-13532.log

...
...

process[rosout-1]: started with pid [13569]
started core service [/rosout]
process[base_link_tof_cam-2]: started with pid [13583]
process[base_link_laser_scan-3]: started with pid [13587]
process[tof/voxl_hal3_tof_cam_ros_node-4]: started with pid [13594]

	DEPTH_IMAGE
	IR: 1 ..... Cloud: 1 ..... Laser: 1
Camera id: -1
Image width: 224
Image height: 1557
Number of frames to dump: 0
Camera mode: preview
SUCCESS: Camera module opened
Camera Id: 0 .. Available raw sizes:
 raw size: 224x1557 (TOF?)
Camera Id: 1 .. Available raw sizes:
 raw size: 1280x480
 raw size: 640x240
User specified TOF camera id = -1.. Autodetecting..
Found TOF camera with ID 0

SUCCESS: TOF interface created!
=========== modalai  Royale3.31, Spectre4.7 CameraDevice::activateUseCase() : return SUCCESS!!
=========== modalai  Royale3.31, Spectre4.7 CameraDevice::activateUseCase() : return SUCCESS!!
Libcamera sending RAW16 TOF data. App calling the PMD libs to postprocess the RAW16 data
[ INFO] [1610597128.592318535]: Loading lens parameters from /data/misc/camera/irs10x0c_lens.cal.
[ INFO] [1610597128.595652985]:
cx/cy     115.439 88.3658
fx/fy     111.314 111.314
tan coeff -0.0020162 -0.00255608
rad coeff -0.259339 0.0933604 -0.0150994

Camera HAL3 ToF camera app is now running

Frame: 0 SensorTimestamp = 4134124857341
Frame: 30 SensorTimestamp = 4135124771341
...
```

## Visualization

### On VOXL
- Run the launch file per instructions above
- Use the VOXL ip address to determine `ROS_MASTER_URI=http://<voxl-ip>:11311/`

### On Desktop
- Determine IP address of the machine using `ifconfig`
```
bash
export ROS_IP=IP-ADDR-OF-PC
export ROS_MASTER_URI=http://<voxl-ip>:11311/
source /opt/ros/<ros-version>/setup.bash
```

The 2 transforms (for both laser scan and point cloud) are in the tof.launch file
    * In RViz choose Fixed Frame toption as `/base_link` instead of `map`
    * Open your .rviz file (by default in `/home/xxx/.rviz/default.rviz`)

```
vi /home/xxx/.rviz/default.rviz
```

- Change the "Fixed Frame:" option from "map" to "base_link"
   - Global Options:
      - Fixed Frame: /base_link

Start the `rviz` program on your desktop (run `rviz` in a terminal)

- On the leftmost column click on the "Add" button
- In the pop-up options click on "Image"
- Change Display Name to "IR-Image"
- In the left column under "IR-Image" tab select type in Image Topic as `/voxl_ir_image_raw`
- Click on the "Add" button again
- In the pop-up options click on "Image"
- Change Display Name to "Depth-Image"
- In the left column under "Depth-Image" tab select type in Image Topic as `/voxl_depth_image_raw`
- "Add" PointCloud2 with Topic as `/voxl_point_cloud`
- "Add" LaserScan with Topic as `/voxl_laser_scan`"`
