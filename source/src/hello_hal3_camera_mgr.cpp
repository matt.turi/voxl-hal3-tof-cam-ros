/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <sys/resource.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <errno.h>
#include <fstream>
#include <iostream>
#include <string>
#include <chrono>
#include <cutils/properties.h>
#include "buffer_manager.h"
#include "hello_hal3_camera.h"
#include "IRImage.hpp"
#include "DepthImage.hpp"
#include "DepthData.hpp"
#include "SparsePointCloud.hpp"

// Main thread functions for request and result processing
void* ThreadPostProcessResult(void* data);
void* ThreadIssueCaptureRequests(void* data);

// -----------------------------------------------------------------------------------------------------------------------------
// Filled in when the camera module sends result image buffers to us. This gets passed to the capture result handling threads's
// message queue
// -----------------------------------------------------------------------------------------------------------------------------
struct CaptureResultFrameData
{
    // Either preview or video or both may be valid
    BufferInfo* pPreviewBufferInfo;     ///< Preview buffer information
    BufferInfo* pVideoBufferInfo;       ///< Video buffer information
    int64_t     timestampNsecs;         ///< Timestamp of the buffer(s) in nano secs
    int         frameNumber;            ///< Frame number associated with the image buffers
};

// -----------------------------------------------------------------------------------------------------------------------------
// Constructor
// -----------------------------------------------------------------------------------------------------------------------------
PerCameraMgr::PerCameraMgr(ros::NodeHandle rosNodeHandle)      ///< ROS node handle
    : m_rosNodeHandle(rosNodeHandle),
        m_rosImageTransport(m_rosNodeHandle)

{
    m_pDevice        = NULL;
    m_previewFormat = 0;
    m_width         = 0;
    m_height        = 0;

    for (uint32_t i = 0; i < StreamTypeMax; i++)
    {
        m_pBufferManager[i] = NULL;
    }

    m_requestThread.pCameraMgr = this;
    m_requestThread.stop       = false;
    m_requestThread.pPrivate   = NULL;
    m_requestThread.msgQueue.clear();

    m_resultThread.pCameraMgr = this;
    m_resultThread.stop       = false;
    m_resultThread.pPrivate   = NULL;
    m_resultThread.msgQueue.clear();
    m_resultThread.lastResultFrameNumber = -1;

    m_pTOFInterface = NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::Initialize(const camera_module_t* pCameraModule,      ///< Camera module
                             const camera_info*     pCameraInfo,        ///< Info about the camera managed by this class
                             int                    cameraid,           ///< Camera id managed
                             int                    width,              ///< Image buffer width that will be streamed
                             int                    height,             ///< Image buffer height that will be streamed
                             PreviewFormat          format,             ///< Image buffer format that will be streamed
                             CameraMode             mode,               ///< Preview / Video mode
                             int                    tofdatatype,        ///< TOF datatype
                             int                    tofnumframes,       ///< Number of TOF frames to dump
                             const char*            pVideoFilename,     ///< Video filename if Video mode is selected
                             int                    dumpPreviewFrames)  ///< Number of preview frames to dump
{
    int status = 0;

    m_pCameraModule                   = pCameraModule;
    m_pCameraInfo                     = pCameraInfo;
    m_cameraId                        = cameraid;
    m_cameraMode                      = mode;
    m_width                           = width;
    m_height                          = height;
    m_tofdatatype                     = tofdatatype;
    m_tofnumframes                    = tofnumframes;
    m_pVideoFilename                  = pVideoFilename;
    m_pVideoFilehandle                = 0;
    m_dumpPreviewFrames               = dumpPreviewFrames;
    m_cameraCallbacks.cameraCallbacks = {CameraModuleCaptureResult, CameraModuleNotify};
    m_cameraCallbacks.pPrivate        = this;
    m_flipImage                       = false;
    m_scanWidthDegrees                = 96;
    m_laserScanLine                   = TofImageHeight / 2;

    DetermineTOFCamera(width, height);

    if (format == PreviewFormatNV21)
    {
        m_previewFormat = HAL_PIXEL_FORMAT_YCbCr_420_888;
    }
    else if (format == PreviewFormatRAW8)
    {
        m_previewFormat = HAL_PIXEL_FORMAT_RAW10;
    }
    else if (format == PreviewFormatBLOB)
    {
        m_previewFormat = HAL_PIXEL_FORMAT_BLOB;
    }

    // Check if the stream configuration is supported by the camera or not. If cameraid doesnt support the stream configuration
    // we just exit. The stream configuration is checked into the static metadata associated with every camera.
    if (IsStreamConfigSupported(m_width, m_height, m_previewFormat) == false)
    {
        status = -EINVAL;
    }

    char cameraName[20] = {0};
    sprintf(cameraName, "%d", m_cameraId);

    if (status == 0)
    {
        status = m_pCameraModule->common.methods->open(&m_pCameraModule->common, cameraName, (hw_device_t**)(&m_pDevice));

        if (status != 0)
        {
            printf("\nOpen camera %s failed!", cameraName);
        }
    }

    if (status == 0)
    {
        status = m_pDevice->ops->initialize(m_pDevice, (camera3_callback_ops*)&m_cameraCallbacks);

        if (status != 0)
        {
            printf("\nInitialize camera %s failed!", cameraName);
        }
    }

    if (status == 0)
    {
        // This calls into the camera module and checks if it supports the stream configuration. If it doesnt then we have to
        // bail out.
        status = ConfigureStreams();
    }

    // Since ConfigureStreams is successful lets allocate the buffer memory since we are definitely going to start processing
    // camera frames now
    if (status == 0)
    {
        status = AllocateStreamBuffers();
    }

    if (status == 0)
    {
        // This is the default metadata i.e. camera settings per request. The camera module passes us the best set of baseline
        // settings. We can modify any setting, for any frame or for every frame, as we see fit.
        ConstructDefaultRequestSettings();
    }

    if (status == 0)
    {
        if (IsTOFCamera())
        {
            // Check param whether libcamera should send use raw or processed TOF data
            // This param should either be omitted or set to 1 using linux cmd "setprop persist.camera.modalai.tof 1"
            // Setting this param to 0 is no longer supported
            int enableAppTofProcessing = 1;
            enableAppTofProcessing = property_get_bool("persist.camera.modalai.tof",(int8_t)enableAppTofProcessing);

            if (enableAppTofProcessing == 1)
            {
                m_pTOFInterface = TOFCreateInterface();

                if (m_pTOFInterface != NULL)
                {
                    printf("\nSUCCESS: TOF interface created!");

                    tof::RoyaleListenerType dataTypes[tof::MaxRoyaleListenerTypes];
                    uint32_t numDataTypes = 0;

                    for (uint32_t i = 0; i < tof::MaxRoyaleListenerTypes; i++)
                    {
                        if ((m_tofdatatype & (1 << i)) != 0)
                        {
                            dataTypes[numDataTypes++] = (tof::RoyaleListenerType)(1 << i);
                        }
                    }

                    TOFInitializationData initializationData = { 0 };

                    initializationData.pTOFInterface = m_pTOFInterface;
                    initializationData.pDataTypes    = &dataTypes[0];
                    initializationData.numDataTypes  = numDataTypes;
                    initializationData.pListener     = this;
                    initializationData.frameRate     = FrameRate;

                    status = TOFInitialize(&initializationData);
                }
                else
                {
                    printf("\nERROR: Cannot initialize create TOF interface!");
                    status = -EINVAL;
                }

                if (status == 0)
                {
                    printf("\nLibcamera sending RAW16 TOF data. App calling the PMD libs to postprocess the RAW16 data\n");
                }
                else
                {
                    printf("\nERROR: Cannot initialize TOF interface to PMD libs!");
                }
            }
            else
            {
                printf("\nERROR: Libcamera sending post-processed TOF data. App does NOT get RAW16 TOF camera data\n");
                status = -EINVAL;
            }
        }
    }

    if (status == 0)
    {
        // Read the values from tof.launch file
        m_rosNodeHandle.param<bool>("publish_depth_image", m_publishDepthImage, false);
        m_rosNodeHandle.param<bool>("publish_ir_image",    m_publishIRImage,    false);
        m_rosNodeHandle.param<bool>("publish_point_cloud", m_publishPointCloud, false);
        m_rosNodeHandle.param<bool>("publish_laser_scan",  m_publishLaserScan,  false);

        m_rosIRImagePublisher    = m_rosImageTransport.advertiseCamera("voxl_ir_image_raw", ImageQueueSize, false);
        m_rosDepthImagePublisher = m_rosImageTransport.advertiseCamera("voxl_depth_image_raw", ImageQueueSize, false);
        m_rosPointCloudPublisher = m_rosNodeHandle.advertise<sensor_msgs::PointCloud2>("voxl_point_cloud", PointCloudChannels);
        m_rosLaserScanPublisher  = m_rosNodeHandle.advertise<sensor_msgs::LaserScan>("voxl_laser_scan", LaseScanQueueSize);

        // Example of how to use params
        // m_rosNodeHandle.param<int>("scan_width_degrees", scan, 11);
        LoadLensParamsFromFile();
        InitializeRosMessages();
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Create the streams that we will use to communicate with the camera module
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::ConfigureStreams()
{
    int status = 0;
    camera3_stream_configuration_t streamConfig = { 0 };

    m_streams[StreamTypePreview].stream_type = CAMERA3_STREAM_OUTPUT;
    m_streams[StreamTypePreview].width       = m_width;
    m_streams[StreamTypePreview].height      = m_height;
    m_streams[StreamTypePreview].format      = m_previewFormat;
    m_streams[StreamTypePreview].data_space  = HAL_DATASPACE_UNKNOWN;
#ifdef USE_GRALLOC1
    m_streams[StreamTypePreview].usage = GRALLOC1_CONSUMER_USAGE_HWCOMPOSER | GRALLOC1_CONSUMER_USAGE_GPU_TEXTURE;
#else
    m_streams[StreamTypePreview].usage = GRALLOC_USAGE_HW_COMPOSER | GRALLOC_USAGE_HW_TEXTURE;
#endif
    m_streams[StreamTypePreview].rotation    = 0;
    m_streams[StreamTypePreview].max_buffers = MaxPreviewBuffers;
    m_streams[StreamTypePreview].priv        = 0;

    m_streams[StreamTypeVideo].stream_type   = CAMERA3_STREAM_OUTPUT;
    m_streams[StreamTypeVideo].width         = m_width;
    m_streams[StreamTypeVideo].height        = m_height;
    m_streams[StreamTypeVideo].format        = HAL_PIXEL_FORMAT_YCbCr_420_888;
    m_streams[StreamTypeVideo].data_space    = HAL_DATASPACE_BT709;
    m_streams[StreamTypeVideo].usage         = GRALLOC_USAGE_HW_VIDEO_ENCODER;
    m_streams[StreamTypeVideo].rotation      = 0;
    m_streams[StreamTypeVideo].max_buffers   = MaxVideoBuffers;
    m_streams[StreamTypeVideo].priv          = 0;

    if (m_cameraMode == CameraModePreview)
    {
        streamConfig.num_streams    = 1;
        streamConfig.operation_mode = QCAMERA3_VENDOR_STREAM_CONFIGURATION_RAW_ONLY_MODE;
    }
    else if (m_cameraMode == CameraModeVideo)
    {
        streamConfig.num_streams    = 2; // Implies preview + video
        streamConfig.operation_mode = CAMERA3_STREAM_CONFIGURATION_NORMAL_MODE;
    }

    camera3_stream_t* pStreams[] = { &m_streams[0], &m_streams[1] };
    streamConfig.streams = &pStreams[0];

    // Call into the camera module to check for support of the required stream config i.e. the required usecase
    status = m_pDevice->ops->configure_streams(m_pDevice, &streamConfig);

    if (status != 0)
    {
        printf("\nHELLOCAMERA-ERROR: Configure streams failed!");
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Allocate the buffers required per stream. Each stream will have its own BufferManager to manage buffers for that stream
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::AllocateStreamBuffers()
{
    int status = 0;

    if (m_cameraMode == CameraModePreview)
    {
        m_pBufferManager[StreamTypePreview] = new BufferManager;

        status = m_pBufferManager[StreamTypePreview]->Initialize(m_streams[StreamTypePreview].max_buffers);

        if (status == 0)
        {
            status = m_pBufferManager[StreamTypePreview]->AllocateBuffers(m_streams[StreamTypePreview].width,
                                                                          m_streams[StreamTypePreview].height,
                                                                          m_streams[StreamTypePreview].format,
                                                                          m_streams[StreamTypePreview].usage,
                                                                          m_streams[StreamTypePreview].usage);
        }
    }
    else if (m_cameraMode == CameraModeVideo)
    {
        m_pBufferManager[StreamTypePreview] = new BufferManager;
        m_pBufferManager[StreamTypeVideo]   = new BufferManager;

        // Allocate preview buffers
        status =  m_pBufferManager[StreamTypePreview]->Initialize(m_streams[StreamTypePreview].max_buffers);

        if (status == 0)
        {
            status = m_pBufferManager[StreamTypePreview]->AllocateBuffers(m_streams[StreamTypePreview].width,
                                                                          m_streams[StreamTypePreview].height,
                                                                          m_streams[StreamTypePreview].format,
                                                                          m_streams[StreamTypePreview].usage,
                                                                          m_streams[StreamTypePreview].usage);
        }

        // Allocate video buffers
        if (status == 0)
        {
            status = m_pBufferManager[StreamTypeVideo]->Initialize(m_streams[StreamTypeVideo].max_buffers);
        }

        if (status == 0)
        {
            status = m_pBufferManager[StreamTypeVideo]->AllocateBuffers(m_streams[StreamTypeVideo].width,
                                                                        m_streams[StreamTypeVideo].height,
                                                                        m_streams[StreamTypeVideo].format,
                                                                        m_streams[StreamTypeVideo].usage,
                                                                        m_streams[StreamTypeVideo].usage);
        }
    }

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Construct default camera settings that will be passed to the camera module to be used for capturing the frames
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::ConstructDefaultRequestSettings()
{
    int fpsRange[] = {30, 30};
    camera3_request_template_t type = CAMERA3_TEMPLATE_PREVIEW;

    if (m_cameraMode == CameraModePreview)
    {
        type = CAMERA3_TEMPLATE_PREVIEW;
    }
    else if (m_cameraMode == CameraModeVideo)
    {
        type = CAMERA3_TEMPLATE_VIDEO_RECORD;
    }

    // Get the default baseline settings
    camera_metadata_t* pDefaultMetadata = (camera_metadata_t *)m_pDevice->ops->construct_default_request_settings(m_pDevice,
                                                                                                                  type);

    // Modify all the settings that we want to
    m_requestMetadata = clone_camera_metadata(pDefaultMetadata);
    m_requestMetadata.update(ANDROID_CONTROL_AE_TARGET_FPS_RANGE, &fpsRange[0], 2);

    uint8_t antibanding = ANDROID_CONTROL_AE_ANTIBANDING_MODE_AUTO;
    m_requestMetadata.update(ANDROID_CONTROL_AE_ANTIBANDING_MODE,&(antibanding),sizeof(antibanding));

    uint8_t afmode = ANDROID_CONTROL_AF_MODE_CONTINUOUS_VIDEO;
    m_requestMetadata.update(ANDROID_CONTROL_AF_MODE, &(afmode), 1);

    uint8_t reqFaceDetectMode =  (uint8_t)ANDROID_STATISTICS_FACE_DETECT_MODE_OFF;
    m_requestMetadata.update(ANDROID_STATISTICS_FACE_DETECT_MODE, &reqFaceDetectMode, 1);

    uint8_t aeMode         = 0;
    // These are some (psuedo random) initial default values
    int     gainTarget     = 200;
    int64_t exposureTarget = 2259763; // nsecs exposure time
    m_requestMetadata.update(ANDROID_CONTROL_AE_MODE, &aeMode, 1);
    m_requestMetadata.update(ANDROID_SENSOR_SENSITIVITY, &gainTarget, 1);
    m_requestMetadata.update(ANDROID_SENSOR_EXPOSURE_TIME, &exposureTarget, 1);  // Exposure time in nsecs

    uint8_t data = (tof::RoyaleListenerType)m_tofdatatype;

    m_requestMetadata.update(ANDROID_TOF_DATA_OUTPUT, &data, 1);
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function opens the camera and starts sending the capture requests
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::Start()
{
    int status = 0;

    m_requestThread.pDevice = m_pDevice;
    m_resultThread.pDevice  = m_pDevice;

    pthread_condattr_t attr;
    pthread_condattr_init(&attr);
    pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
    pthread_mutex_init(&m_requestThread.mutex, NULL);
    pthread_mutex_init(&m_resultThread.mutex, NULL);
    pthread_cond_init(&m_requestThread.cond, &attr);
    pthread_cond_init(&m_resultThread.cond, &attr);
    pthread_condattr_destroy(&attr);

    // Start the thread that will process the camera capture result. This thread wont exit till it consumes all expected
    // output buffers from the camera module
    pthread_attr_t resultAttr;
    pthread_attr_init(&resultAttr);
    pthread_attr_setdetachstate(&resultAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&(m_resultThread.thread), &resultAttr, ThreadPostProcessResult, &m_resultThread);
    pthread_attr_destroy(&resultAttr);

    // Start the thread that will send the camera capture request. This thread wont stop issuing requests to the camera
    // module until we terminate the program with Ctrl+C
    pthread_attr_t requestAttr;
    pthread_attr_init(&requestAttr);
    pthread_attr_setdetachstate(&requestAttr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&(m_requestThread.thread), &requestAttr, ThreadIssueCaptureRequests, &m_requestThread);
    pthread_attr_destroy(&resultAttr);

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function stops the camera and does all necessary clean up
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::Stop()
{
    ///< @todo Need to wait for all Royale postprocessed frames to return. We may have submitted frames to Royale for processing
    ///        that we may not have received back. We need to bail out elegantly waiting for Royale to finish.

    // The result thread will stop when the result of the last frame is received
    m_requestThread.stop = true;

    pthread_join(m_requestThread.thread, NULL);
    pthread_cond_signal(&m_requestThread.cond);
    pthread_mutex_unlock(&m_requestThread.mutex);
    pthread_mutex_destroy(&m_requestThread.mutex);
    pthread_cond_destroy(&m_requestThread.cond);

    pthread_join(m_resultThread.thread, NULL);
    pthread_cond_signal(&m_resultThread.cond);
    pthread_mutex_unlock(&m_resultThread.mutex);
    pthread_mutex_destroy(&m_resultThread.mutex);
    pthread_cond_destroy(&m_resultThread.cond);

    if (m_pDevice != NULL)
    {
        m_pDevice->common.close(&m_pDevice->common);

        if (m_pVideoFilehandle > 0)
        {
            fclose(m_pVideoFilehandle);
        }

        m_pDevice = NULL;
    }

    for (uint32_t i = 0; i < StreamTypeMax; i++)
    {
        if (m_pBufferManager[i] != NULL)
        {
            delete m_pBufferManager[i];
            m_pBufferManager[i] = NULL;
        }
    }

    if (m_pTOFInterface)
    {
        TOFDestroyInterface(m_pTOFInterface);
        m_pTOFInterface = NULL;
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Check if the stream resolution, format is supported in the camera static characteristics
// -----------------------------------------------------------------------------------------------------------------------------
bool PerCameraMgr::IsStreamConfigSupported(int width, int height, int format)
{
    bool isStreamSupported = false;

    if (m_pCameraInfo != NULL)
    {
        camera_metadata_t* pStaticMetadata = (camera_metadata_t *)m_pCameraInfo->static_camera_characteristics;
        camera_metadata_ro_entry entry;

        // Get the list of all stream resolutions supported and then go through each one of them looking for a match
        int status = find_camera_metadata_ro_entry(pStaticMetadata, ANDROID_SCALER_AVAILABLE_STREAM_CONFIGURATIONS, &entry);

        if ((0 == status) && (0 == (entry.count % 4)))
        {
            for (size_t i = 0; i < entry.count; i+=4)
            {
                if (ANDROID_SCALER_AVAILABLE_STREAM_CONFIGURATIONS_OUTPUT == entry.data.i32[i + 3])
                {
                    if ((format == entry.data.i32[i])   &&
                        (width  == entry.data.i32[i+1]) &&
                        (height == entry.data.i32[i+2]))
                    {
                        isStreamSupported = true;
                        break;
                    }
                }
            }
        }
    }

    if (isStreamSupported == false)
    {
        printf("\nCamera Width: %d, Height: %d, Format: %d not supported!", width, height, format);
    }

    return isStreamSupported;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Function that will process one capture result sent from the camera module. Remember this function is operating in the camera
// module thread context. So we do the bare minimum work that we need to do and return control back to the camera module. The
// bare minimum we do is to dispatch the work to another worker thread who consumes the image buffers passed to it from here.
// Our result worker thread is "ThreadPostProcessResult(..)"
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::ProcessOneCaptureResult(const camera3_capture_result* pHalResult)
{
    if (pHalResult->result != NULL)
    {
        camera_metadata_ro_entry entry;
        int64_t sensorTimestamp = 0;
        int     result          = 0;

        result = find_camera_metadata_ro_entry(pHalResult->result, ANDROID_SENSOR_TIMESTAMP, &entry);

        if ((0 == result) && (entry.count > 0))
        {
            sensorTimestamp = entry.data.i64[0];

            if ((pHalResult->frame_number % 30) == 0)
            {
                printf("\nFrame: %d SensorTimestamp = \t %lld", pHalResult->frame_number, sensorTimestamp);
            }
        }
    }

    if (pHalResult->num_output_buffers > 0)
    {
        CaptureResultFrameData* pCaptureResultData = new CaptureResultFrameData;

        // The camera frames is from UKNOWN_SOURCE timestamp. Therefore use CLOCK_REALTIME for camera frames timestamp in case
        // we want to sync camera frame timestamp with some other data also in CLOCK_REALTIME.
        int64_t imageTimestampNsecs;
        static struct timespec temp;
        clock_gettime(CLOCK_REALTIME, &temp);
        imageTimestampNsecs = (temp.tv_sec*1e9) + temp.tv_nsec;

        memset(pCaptureResultData, 0, sizeof(CaptureResultFrameData));

        pCaptureResultData->frameNumber    = pHalResult->frame_number;
        pCaptureResultData->timestampNsecs = imageTimestampNsecs;

        // Go through all the output buffers received. It could be preview only, video only, or preview + video
        for (uint32_t i = 0; i < pHalResult->num_output_buffers; i++)
        {
            buffer_handle_t* pImageBuffer;

            if (pHalResult->output_buffers[i].stream == &m_streams[StreamTypePreview])
            {
                pImageBuffer = pHalResult->output_buffers[i].buffer;
                pCaptureResultData->pPreviewBufferInfo = m_pBufferManager[StreamTypePreview]->GetBufferInfo(pImageBuffer);
                m_pBufferManager[StreamTypePreview]->PutBuffer(pImageBuffer); // This queues up the buffer for recycling
            }
            else if (pHalResult->output_buffers[i].stream == &m_streams[StreamTypeVideo])
            {
                pImageBuffer = pHalResult->output_buffers[i].buffer;
                pCaptureResultData->pVideoBufferInfo = m_pBufferManager[StreamTypeVideo]->GetBufferInfo(pImageBuffer);
                m_pBufferManager[StreamTypeVideo]->PutBuffer(pImageBuffer); // This queues up the buffer for recycling
            }
        }

        // Mutex is required for msgQueue access from here and from within the thread wherein it will be de-queued
        pthread_mutex_lock(&m_resultThread.mutex);
        // Queue up work for the result thread "ThreadPostProcessResult"
        m_resultThread.msgQueue.push_back((void*)pCaptureResultData);
        pthread_cond_signal(&m_resultThread.cond);
        pthread_mutex_unlock(&m_resultThread.mutex);
    }
}
// -----------------------------------------------------------------------------------------------------------------------------
// Process the result from the camera module. Essentially handle the metadata and the image buffers that are sent back to us.
// We call the PerCameraMgr class function to handle it so that it can have access to any (non-static)class member data it needs
// Remember this function is operating in the camera module thread context. So we should do the bare minimum work and return.
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::CameraModuleCaptureResult(const camera3_callback_ops *cb, const camera3_capture_result* pHalResult)
{
    Camera3Callbacks* pCamera3Callbacks = (Camera3Callbacks*)cb;
    PerCameraMgr* pPerCameraMgr = (PerCameraMgr*)pCamera3Callbacks->pPrivate;

    pPerCameraMgr->ProcessOneCaptureResult(pHalResult);
}

// -----------------------------------------------------------------------------------------------------------------------------
// Handle any messages sent to us by the camera module
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::CameraModuleNotify(const struct camera3_callback_ops *cb, const camera3_notify_msg_t *msg)
{
    if (msg->type == CAMERA3_MSG_ERROR)
    {
        printf("\nHELLOCAMERA-ERROR: Framenumber: %d ErrorCode: %d",
               msg->message.error.frame_number, msg->message.error.error_code);
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Send one capture request to the camera module
// -----------------------------------------------------------------------------------------------------------------------------
int PerCameraMgr::ProcessOneCaptureRequest(int frameNumber)
{
    int status = 0;
    camera3_capture_request_t request = { 0 };
    camera3_stream_buffer_t   streamBuffers[StreamTypeMax];

    if (m_cameraMode == CameraModePreview)
    {
        streamBuffers[0].buffer        = (const native_handle_t**)(m_pBufferManager[StreamTypePreview]->GetBuffer());
        streamBuffers[0].stream        = &m_streams[StreamTypePreview];
        streamBuffers[0].status        = 0;
        streamBuffers[0].acquire_fence = -1;
        streamBuffers[0].release_fence = -1;

        request.num_output_buffers = 1;
        request.output_buffers     = &streamBuffers[0];
    }
    else if (m_cameraMode == CameraModeVideo)
    {
        streamBuffers[0].buffer        = (const native_handle_t**)(m_pBufferManager[StreamTypePreview]->GetBuffer());
        streamBuffers[0].stream        = &m_streams[StreamTypePreview];
        streamBuffers[0].status        = 0;
        streamBuffers[0].acquire_fence = -1;
        streamBuffers[0].release_fence = -1;

        streamBuffers[1].buffer        = (const native_handle_t**)(m_pBufferManager[StreamTypeVideo]->GetBuffer());
        streamBuffers[1].stream        = &m_streams[StreamTypeVideo];
        streamBuffers[1].status        = 0;
        streamBuffers[1].acquire_fence = -1;
        streamBuffers[1].release_fence = -1;

        request.num_output_buffers = 2;
        request.output_buffers     = &streamBuffers[0];
    }

    request.frame_number = frameNumber;
    request.settings     = m_requestMetadata.getAndLock();
    request.input_buffer = NULL;

    // Call the camera module to send the capture request
    status = m_pDevice->ops->process_capture_request(m_pDevice, &request);

    if (status != 0)
    {
        printf("\nHELLOCAMERA-ERROR: Error sending request %d", frameNumber);
    }

    m_requestMetadata.unlock(request.settings);

    return status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// PerCameraMgr::CameraModuleCaptureResult(..) is the entry callback that is registered with the camera module to be called when
// the camera module has frame result available to be processed by this application. We do not want to do much processing in
// that function since it is being called in the context of the camera module. So we do the bare minimum processing and leave
// the remaining process upto this function. PerCameraMgr::CameraModuleCaptureResult(..) just pushes a message in a queue that
// is monitored by this thread function. This function goes through the message queue and processes all the messages in it. The
// messages are nothing but camera images that this application has received.
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadPostProcessResult(void* pData)
{
    // Set thread priority
    pid_t tid = syscall(SYS_gettid);
    int which = PRIO_PROCESS;
    int nice  = -10;

    setpriority(which, tid, nice);

    ThreadData*   pThreadData       = (ThreadData*)pData;
    int           dumpPreviewFrames = pThreadData->pCameraMgr->GetDumpPreviewFrames();
    void*         pTOFInterface     = pThreadData->pCameraMgr->GetTOFInterface();
    int           frame_number      = 0;
    uint8_t*      pRaw8bit          = NULL;

    // The condition of the while loop is such that this thread will not terminate till it receives the last expected image
    // frame from the camera module
    while (pThreadData->lastResultFrameNumber != frame_number)
    {
        pthread_mutex_lock(&pThreadData->mutex);

        if (pThreadData->msgQueue.empty())
        {
            struct timespec tv;
            clock_gettime(CLOCK_MONOTONIC, &tv);
            tv.tv_sec += 1;

            // Go to a temporary small sleep waiting for the result frame to arrive
            if(pthread_cond_timedwait(&pThreadData->cond, &pThreadData->mutex, &tv) != 0)
            {
                pthread_mutex_unlock(&pThreadData->mutex);
                continue;
            }
        }

        // Coming here means we have a result frame to process

        CaptureResultFrameData* pCaptureResultData = (CaptureResultFrameData*)pThreadData->msgQueue.front();

        pThreadData->msgQueue.pop_front();
        pthread_mutex_unlock(&pThreadData->mutex);

        // Handle dumping preview frames to files
        if (pCaptureResultData->pPreviewBufferInfo != NULL)
        {
            BufferInfo* pBufferInfo     = pCaptureResultData->pPreviewBufferInfo;
            uint8_t*    pImagePixels    = (uint8_t*)pBufferInfo->vaddr;
            static int  previewFrameNum = 0;

            char filename[256] = { '\0' };

            if (pTOFInterface != NULL)
            {
                TOFProcessRAW16(pTOFInterface,
                                (uint16_t*)pCaptureResultData->pPreviewBufferInfo->vaddr,
                                pCaptureResultData->timestampNsecs);
            }

            if (dumpPreviewFrames > 0)
            {
                dumpPreviewFrames--;

                if (pBufferInfo->format == HAL_PIXEL_FORMAT_RAW10)
                {
                    sprintf(&filename[0], "%s_%d.yuv", "image_preview_raw", previewFrameNum++);

                    uint32_t imageSizePixels = pBufferInfo->width * pBufferInfo->height;

                    if (pRaw8bit == NULL)
                    {
                        pRaw8bit = (uint8_t*)malloc(imageSizePixels  * 1.25); // 1.25 because its 10 bits/pixel
                    }

                    uint8_t* pSrcPixel  = (uint8_t*)pBufferInfo->vaddr;
                    uint8_t* pDestPixel = pRaw8bit;
                    pImagePixels        = pRaw8bit;

                    // This link has the description of the RAW10 format:
                    // https://gitlab.com/SaberMod/pa-android-frameworks-base/commit/d1988a98ed69db8c33b77b5c085ab91d22ef3bbc
                    for (unsigned int i = 0; i < imageSizePixels; i+=4)
                    {
                        uint8_t* pFifthByte = pSrcPixel + 4;
                        uint8_t  pixel0     = ((*pFifthByte & 0xC0) >> 6);
                        uint8_t  pixel1     = ((*pFifthByte & 0x30) >> 4);
                        uint8_t  pixel2     = ((*pFifthByte & 0x0C) >> 2);
                        uint8_t  pixel3     = ((*pFifthByte & 0x03) >> 0);
                        uint32_t temp;
                        uint32_t Max = 255;
                        uint32_t tempPixel;

                        tempPixel = *pSrcPixel;
                        tempPixel <<= 2;
                        temp = tempPixel+pixel0;
                        *pDestPixel = (uint8_t)std::min(Max, temp);
                        pDestPixel++;
                        pSrcPixel++;

                        tempPixel = *pSrcPixel;
                        tempPixel <<= 2;
                        temp = tempPixel+pixel1;
                        *pDestPixel = (uint8_t)std::min(Max, temp);
                        pDestPixel++;
                        pSrcPixel++;

                        tempPixel = *pSrcPixel;
                        tempPixel <<= 2;
                        temp = tempPixel+pixel2;
                        *pDestPixel = (uint8_t)std::min(Max, temp);
                        pDestPixel++;
                        pSrcPixel++;

                        tempPixel = *pSrcPixel;
                        tempPixel <<= 2;
                        temp = tempPixel+pixel3;
                        *pDestPixel = (uint8_t)std::min(Max, temp);
                        pDestPixel++;
                        pSrcPixel++;

                        pSrcPixel++;
                    }

                    FILE* fd = fopen(&filename[0], "wb");
                    fwrite(pImagePixels, pCaptureResultData->pPreviewBufferInfo->size, 1, fd);
                    fclose(fd);
                }
                else if (pBufferInfo->format == HAL_PIXEL_FORMAT_BLOB)
                {
                    sprintf(&filename[0], "%s_%d.yuv", "image_preview_blob", previewFrameNum++);

                    FILE* fd = fopen(&filename[0], "wb");
                    fclose(fd);
                    fd = fopen(&filename[0], "a");

                    pImagePixels = (uint8_t*)pBufferInfo->vaddr;
                    fwrite(pImagePixels, pCaptureResultData->pPreviewBufferInfo->size, 1, fd);
                    fclose(fd);
                }
                else
                {
                    sprintf(&filename[0], "%s_%d.yuv", "image_preview_nv21", previewFrameNum++);

                    FILE* fd = fopen(&filename[0], "wb");
                    fclose(fd);
                    fd = fopen(&filename[0], "a");
                    pImagePixels = (uint8_t*)pBufferInfo->vaddr;
                    fwrite(pImagePixels, (pBufferInfo->width * pBufferInfo->height), 1, fd);
                    pImagePixels = (uint8_t*)pBufferInfo->craddr;
                    // CbCr are subsampled so that height of the UV plane is UV/2
                    fwrite(pImagePixels, (pBufferInfo->width * (pBufferInfo->height/2)), 1, fd);
                    fclose(fd);
                }
            }
        }

        delete pCaptureResultData;

        frame_number = pCaptureResultData->frameNumber;
    }

    if (pRaw8bit != NULL)
    {
        free(pRaw8bit);
    }

    printf("\n========= Last result frame: %d", frame_number);
    fflush(stdout);

    return NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Main thread function to initiate the sending of capture requests to the camera module. Keeps on sending the capture requests
// to the camera module till a "stop message" is passed to this thread function
// -----------------------------------------------------------------------------------------------------------------------------
void* ThreadIssueCaptureRequests(void* data)
{
    ThreadData*   pThreadData = (ThreadData*)data;
    PerCameraMgr* pCameraMgr  = pThreadData->pCameraMgr;
    // Set thread priority
    pid_t tid = syscall(SYS_gettid);
    int which = PRIO_PROCESS;
    int nice  = -10;

    int frame_number = -1;

    setpriority(which, tid, nice);

    while (!pThreadData->stop)
    {
        pCameraMgr->ProcessOneCaptureRequest(++frame_number);
    }

    // Stop message received. Inform about the last framenumber requested from the camera module. This in turn will be used
    // by the result thread to wait for this frame's image buffers to arrive.
    pCameraMgr->StoppedSendingRequest(frame_number);
    printf("\n========= Last request frame: %d", frame_number);
    return NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// This function is called to indicate that the request sending thread has issued the last request and no more capture requests
// will be sent
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::StoppedSendingRequest(int framenumber)
{
    m_resultThread.lastResultFrameNumber = framenumber;
}

// -----------------------------------------------------------------------------------------------------------------------------
// The TOF library calls this function when it receives data from the Royale PMD libs
// -----------------------------------------------------------------------------------------------------------------------------
bool PerCameraMgr::RoyaleDataDone(const void*             pData,
                                  uint32_t                size,
                                  int64_t                 timestamp,
                                  tof::RoyaleListenerType dataType)
{
    char  fileName[256] = {0};
    FILE* fp = NULL;
    /// <@todo This could be changed to anything - make it configurable
    static const int DisplayFrequency = 4;

    if (dataType == tof::RoyaleListenerType::LISTENER_IR_IMAGE)
    {
        static int frameNum = 0;

        if (frameNum % DisplayFrequency == 0)
        {
            PublishIRImage(pData);
        }

        if (frameNum < m_tofnumframes)
        {
            sprintf(fileName, "IR_Image_%d.yuv", frameNum);
            fp = fopen(fileName, "w");
            fclose(fp);
            fp = fopen(fileName, "a");

            const royale::IRImage *data = static_cast<const royale::IRImage *> (pData);

            fwrite((uint8_t*)&data->timestamp, sizeof (data->timestamp), 1, fp);
            fwrite((uint8_t*)&data->streamId, sizeof (data->streamId), 1, fp);
            fwrite((uint8_t*)&data->width, sizeof (data->width), 1, fp);
            fwrite((uint8_t*)&data->height, sizeof (data->height), 1, fp);
            fwrite((uint8_t *)(data->data.data()), data->data.size() * sizeof(uint8_t), 1, fp);

            fclose(fp);
        }

        frameNum++;
    }
    else if (dataType == tof::RoyaleListenerType::LISTENER_DEPTH_IMAGE)
    {
        static int frameNum = 0;

        if (frameNum % DisplayFrequency == 0)
        {
            PublishDepthImage(pData);
        }

        if (frameNum < m_tofnumframes)
        {
            sprintf(fileName, "Depth_Image_%d.yuv", frameNum);
            fp = fopen(fileName, "w");
            fclose(fp);
            fp = fopen(fileName, "a");

            const royale::DepthImage *data = static_cast<const royale::DepthImage *> (pData);

            fwrite((uint8_t*)&data->timestamp, sizeof (data->timestamp), 1, fp);
            fwrite((uint8_t*)&data->streamId, sizeof (data->streamId), 1, fp);
            fwrite((uint8_t*)&data->width, sizeof (data->width), 1, fp);
            fwrite((uint8_t*)&data->height, sizeof (data->height), 1, fp);
            fwrite((uint8_t *)(data->cdData.data()), data->cdData.size() * sizeof(uint16_t), 1, fp);

            fclose(fp);
        }

        frameNum++;
    }
    else if (dataType == tof::RoyaleListenerType::LISTENER_SPARSE_POINT_CLOUD)
    {
        static int frameNum = 0;

        if (frameNum < m_tofnumframes)
        {
            sprintf(fileName, "Sparse_Point_Cloud_%d.raw", frameNum);
            fp = fopen(fileName, "w");
            fclose(fp);
            fp = fopen(fileName, "a");

            const royale::SparsePointCloud *data = static_cast<const royale::SparsePointCloud *> (pData);

            fwrite((uint8_t*)&data->timestamp, sizeof (data->timestamp), 1, fp);
            fwrite((uint8_t*)&data->streamId, sizeof (data->streamId), 1, fp);
            fwrite((uint8_t*)&data->numPoints, sizeof (data->numPoints), 1, fp);
            fwrite((uint8_t*)&data->xyzcPoints[0], data->xyzcPoints.size() * sizeof(float), 1, fp);

            fclose(fp);
        }

        frameNum++;
    }
    else if (dataType == tof::RoyaleListenerType::LISTENER_DEPTH_DATA)
    {
        static int frameNum = 0;

        if (frameNum % DisplayFrequency == 0)
        {
            PublishDepthData(pData, timestamp);
        }

        if (frameNum < m_tofnumframes)
        {
            sprintf(fileName, "Depth_Data_%d.raw", frameNum);
            fp = fopen(fileName, "w");
            fclose(fp);
            fp = fopen(fileName, "a");

            const royale::DepthData *data = static_cast<const royale::DepthData *> (pData);
            int size_points = data->points.size() * sizeof(royale::DepthPoint);

            fwrite((uint8_t*)&data->width, sizeof (data->width), 1, fp);
            fwrite((uint8_t*)&data->height, sizeof (data->height), 1, fp);
            fwrite((uint8_t*)&size_points, sizeof(size_points), 1, fp);
            fwrite((uint8_t*)&data->points[0], size_points, 1, fp);

            fclose(fp);
        }

        frameNum++;
    }

    return true;
}


// -----------------------------------------------------------------------------------------------------------------------------
// Initialize ROS message structures
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::InitializeRosMessages()
{
    std::string frame_id = std::string("hal3_tof_camera");

    InitializeCameraInfoMessage(TofImageHeight, TofImageWidth, frame_id);
    InitializeDepthMessage(TofImageHeight, TofImageWidth, frame_id);
    InitializeIrImageMessage(TofImageHeight, TofImageWidth, frame_id);
    InitializePointCloudMessage(TofImageHeight, TofImageWidth, frame_id);
    InitializeLaserScanMessage(std::string("laser_scan"));
}

// -----------------------------------------------------------------------------------------------------------------------------
// Initialize camera info message
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::InitializeCameraInfoMessage(int imageHeight, int imageWidth, std::string frame_id)
{
    // load lens params from EEPROM
    double fx   = m_lensParameters.focalLength.first;
    double fy   = m_lensParameters.focalLength.second;
    double rad0 = m_lensParameters.distortionRadial[0];
    double rad1 = m_lensParameters.distortionRadial[1];
    double rad2 = m_lensParameters.distortionRadial[2];

    // when flipping the image, tangential distortion must be flipped
    // and primciple point reflected over the center of the image
    double px, py, tan0, tan1;

    if(m_flipImage == true)
    {
        px   = TofImageWidth  - m_lensParameters.principalPoint.first;
        py   = TofImageHeight - m_lensParameters.principalPoint.second;
        tan0 = -m_lensParameters.distortionTangential.first;
        tan1 = -m_lensParameters.distortionTangential.second;
    }
    else
    {
        px   = m_lensParameters.principalPoint.first;
        py   = m_lensParameters.principalPoint.second;
        tan0 = m_lensParameters.distortionTangential.first;
        tan1 = m_lensParameters.distortionTangential.second;
    }

    // populate cameraInfoMsg for ROS
    m_cameraInfoMsg.header.frame_id = frame_id;
    m_cameraInfoMsg.width           = imageWidth;
    m_cameraInfoMsg.height          = imageHeight;

    // distortion parameters. 5-param radtan model identical to factory sensor cal
    m_cameraInfoMsg.distortion_model = "plumb_bob";
    m_cameraInfoMsg.D.resize(5);
    m_cameraInfoMsg.D[0] = rad0;
    m_cameraInfoMsg.D[1] = rad1;
    m_cameraInfoMsg.D[2] = tan0;
    m_cameraInfoMsg.D[3] = tan1;
    m_cameraInfoMsg.D[4] = rad2;

    // Intrinsic camera matrix for the raw (distorted) images.
    m_cameraInfoMsg.K[0] = fx;
    m_cameraInfoMsg.K[1] = 0;
    m_cameraInfoMsg.K[2] = px;
    m_cameraInfoMsg.K[3] = 0;
    m_cameraInfoMsg.K[4] = fy;
    m_cameraInfoMsg.K[5] = py;
    m_cameraInfoMsg.K[6] = 0;
    m_cameraInfoMsg.K[7] = 0;
    m_cameraInfoMsg.K[8] = 1;

    // Rectification matrix STEREO CAM ONLY, leave as identity for monocular
    m_cameraInfoMsg.R[0] = 1;
    m_cameraInfoMsg.R[1] = 0;
    m_cameraInfoMsg.R[2] = 0;
    m_cameraInfoMsg.R[3] = 0;
    m_cameraInfoMsg.R[4] = 1;
    m_cameraInfoMsg.R[5] = 0;
    m_cameraInfoMsg.R[6] = 0;
    m_cameraInfoMsg.R[7] = 0;
    m_cameraInfoMsg.R[8] = 1;

    // Projection matrix
    m_cameraInfoMsg.P[0] = fx;
    m_cameraInfoMsg.P[1] = 0;
    m_cameraInfoMsg.P[2] = px;
    m_cameraInfoMsg.P[3] = 0;  // Tx for stereo offset
    m_cameraInfoMsg.P[4] = 0;
    m_cameraInfoMsg.P[5] = fy;
    m_cameraInfoMsg.P[6] = py;
    m_cameraInfoMsg.P[7] = 0;  // Ty for stereo offset
    m_cameraInfoMsg.P[8] = 0;
    m_cameraInfoMsg.P[9] = 0;
    m_cameraInfoMsg.P[10] = 1;
    m_cameraInfoMsg.P[11] = 0;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Initialize depth message
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::InitializeDepthMessage(int imageHeight, int imageWidth, std::string frame_id)
{
    m_depthImageMsg.header.frame_id = frame_id;
    m_depthImageMsg.width           = imageWidth;
    m_depthImageMsg.height          = imageHeight;
    m_depthImageMsg.is_bigendian    = false;
    m_depthImageMsg.encoding        = sensor_msgs::image_encodings::TYPE_16UC1;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Initialize IR image message
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::InitializeIrImageMessage(int imageHeight, int imageWidth, std::string frame_id)
{
    m_irImageMsg.header.frame_id = frame_id;
    m_irImageMsg.width           = imageWidth;
    m_irImageMsg.height          = imageHeight;
    m_irImageMsg.is_bigendian    = false;
    m_irImageMsg.encoding        = sensor_msgs::image_encodings::MONO8;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Initialize point cloud message
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::InitializePointCloudMessage(int imageHeight, int imageWidth, std::string frame_id)
{
    m_pointCloudMsg.header.frame_id = frame_id;
    m_pointCloudMsg.width           = imageWidth;
    m_pointCloudMsg.height          = imageHeight;

    m_pointCloudMsg.is_bigendian = false;
    // Data is not "dense" since not all points are valid.
    m_pointCloudMsg.is_dense     = false;

    // The total size of the channels of the point cloud for a single point are:
    // sizeof(float) * 3 = x, y, and z
    // sizeof(float) = noise
    // sizeof(uint32_t) = depthConfidence (only the low byte is defined, using uin32_t for alignment)
    m_pointCloudMsg.point_step = sizeof(float) * 3 + sizeof(float) + sizeof(uint32_t) + sizeof(uint16_t);
    m_pointCloudMsg.row_step   = 1;

    // Describe the fields (channels) associated with each point.
    m_pointCloudMsg.fields.resize(PointCloudChannels);
    m_pointCloudMsg.fields[0].name = "x";
    m_pointCloudMsg.fields[1].name = "y";
    m_pointCloudMsg.fields[2].name = "z";
    m_pointCloudMsg.fields[3].name = "noise";

    // Defines the format of x, y, z, and noise channels.
    size_t index;

    for (index = 0; index < 4; index++)
    {
        m_pointCloudMsg.fields[index].offset = sizeof(float) * index;
        m_pointCloudMsg.fields[index].datatype = sensor_msgs::PointField::FLOAT32;
        m_pointCloudMsg.fields[index].count = 1;
    }

    // Defines the format of the depthConfidence channel.
    m_pointCloudMsg.fields[index].name     = "confidence";
    m_pointCloudMsg.fields[index].offset   = m_pointCloudMsg.fields[index - 1].offset + sizeof(uint32_t);
    m_pointCloudMsg.fields[index].datatype = sensor_msgs::PointField::UINT32;
    m_pointCloudMsg.fields[index].count    = 1;

    index++;
    m_pointCloudMsg.fields[index].name     = "intensity";
    m_pointCloudMsg.fields[index].offset   = m_pointCloudMsg.fields[index - 1].offset + sizeof(uint16_t);
    m_pointCloudMsg.fields[index].datatype = sensor_msgs::PointField::UINT16;
    m_pointCloudMsg.fields[index].count    = 1;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Initialize Laser scan message
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::InitializeLaserScanMessage(std::string frame_id)
{
    m_laserScanMsg.header.frame_id = frame_id;

    // scan_width_degrees is a ros argument
    // 96 for wide Sunny lens, 62 for narrow Infineon lens
    float scan_angle_rad = (m_scanWidthDegrees * M_PI) / 180.0;
    float start_angle    = 0.0;
    m_laserScanMsg.angle_min       = start_angle - (scan_angle_rad / 2.0);
    m_laserScanMsg.angle_max       = start_angle + (scan_angle_rad / 2.0);
    m_laserScanMsg.angle_increment = scan_angle_rad / ((float)TofImageWidth);
    m_laserScanMsg.time_increment  = 0.0;
    m_laserScanMsg.scan_time       = 0.3333;
    m_laserScanMsg.range_min       = 0.0001;
    m_laserScanMsg.range_max       = 25.0;
}

// -----------------------------------------------------------------------------------------------------------------------------
// IR Image publish function
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::PublishIRImage(const void* pData)
{
    const royale::IRImage* pIRData = static_cast<const royale::IRImage *> (pData);

    m_irImageMsg.header.stamp.fromNSec(pIRData->timestamp);
    m_cameraInfoMsg.header.stamp.fromNSec(pIRData->timestamp);

    m_irImageMsg.width  = pIRData->width;
    m_irImageMsg.height = pIRData->height;
    m_irImageMsg.step   = pIRData->width;

    uint32_t totalBytes = m_irImageMsg.height * m_irImageMsg.step;
    m_irImageMsg.data.resize(totalBytes);

    memcpy(&m_irImageMsg.data[0], (uint8_t *)(pIRData->data.data()), totalBytes);

    m_irImageMsg.is_bigendian = false;

    // static int publishedFrame = 0;
    // printf("\nPublishing IR image %d: W:%d H:%d S:%d",
    //           publishedFrame++, m_irImageMsg.width, m_irImageMsg.height, m_irImageMsg.step);
    m_rosIRImagePublisher.publish(m_irImageMsg, m_cameraInfoMsg);
}

// -----------------------------------------------------------------------------------------------------------------------------
// Depth Image publish function
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::PublishDepthImage(const void* pData)
{
    const royale::DepthImage* pDepthImageData = static_cast<const royale::DepthImage *> (pData);

    m_depthImageMsg.header.stamp.fromNSec(pDepthImageData->timestamp);
    m_cameraInfoMsg.header.stamp.fromNSec(pDepthImageData->timestamp);

    m_depthImageMsg.width  = pDepthImageData->width;
    m_depthImageMsg.height = pDepthImageData->height;
    m_depthImageMsg.step   = pDepthImageData->width * 2; // 2 because depth image is 16bytes per pixel

    uint32_t totalBytes = m_depthImageMsg.height * m_depthImageMsg.step; // 2 because depth image is 16bytes per pixel
    m_depthImageMsg.data.resize(totalBytes);

    uint16_t* pDepthImage  = (uint16_t*)&m_depthImageMsg.data[0];
    uint16_t* pRoyaleImage = (uint16_t*)(pDepthImageData->cdData.data());

    for(size_t i=0; i < pDepthImageData->cdData.size(); i++)
    {
        *pDepthImage = ((*pRoyaleImage) & 0x1FFF);
        pDepthImage++;
        pRoyaleImage++;
    }

    m_depthImageMsg.is_bigendian = false;

    // static int publishedFrame = 0;
    // printf("\nPublishing Depth image %d: W:%d H:%d S:%d",
    //          publishedFrame++, m_depthImageMsg.width, m_depthImageMsg.height, m_depthImageMsg.step);
    m_rosDepthImagePublisher.publish(m_depthImageMsg, m_cameraInfoMsg);
}

// -----------------------------------------------------------------------------------------------------------------------------
// Depth data publish function
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::PublishDepthData(const void* pData, int64_t timestamp)
{
    const royale::DepthData* pDepthData = static_cast<const royale::DepthData *> (pData);
    const royale::Vector<royale::DepthPoint>& pointIn = pDepthData->points;

    m_depthImageMsg.header.stamp.fromNSec(timestamp);
    m_pointCloudMsg.header.stamp.fromNSec(timestamp);
    m_laserScanMsg.header.stamp.fromNSec(timestamp);
    m_irImageMsg.header.stamp.fromNSec(timestamp);
    m_cameraInfoMsg.header.stamp.fromNSec(timestamp);

    m_irImageMsg.width  = pDepthData->width;
    m_irImageMsg.height = pDepthData->height;
    m_irImageMsg.step   = pDepthData->width;
    m_irImageMsg.data.resize(m_irImageMsg.width * m_irImageMsg.height);

    // Define the dimensions of this point cloud based on the number data points received.
    m_pointCloudMsg.width    = pDepthData->width;
    m_pointCloudMsg.height   = pDepthData->height;
    m_pointCloudMsg.row_step = m_pointCloudMsg.point_step * pDepthData->width;
    m_pointCloudMsg.data.resize(m_pointCloudMsg.height * m_pointCloudMsg.row_step);

    m_depthImageMsg.width  = pDepthData->width;
    m_depthImageMsg.height = pDepthData->height;
    m_depthImageMsg.step   = pDepthData->width * 2; // 2 because depth image is 2 bytes per pixel
    m_depthImageMsg.data.resize(m_depthImageMsg.height * m_depthImageMsg.step);

    ///<@todo Get rid of magic numbers
    // Set up the Laser Scan message. Pre-populate all ranges to infinity.
    int rowOffset = 223;
    m_laserScanMsg.ranges.assign(224, std::numeric_limits<double>::infinity());

    size_t numPoints = pointIn.size();

    for (size_t pointIndex = 0; pointIndex < numPoints; ++pointIndex)
    {
        ///<@todo Fix depthConfidence logic
        // if (m_publishDepthImage)
        // {
        //     int depthInMm = pointIn[pointIndex].z * 1000;
        //     // If Confidence level is too low, ignore this depth level.
        //     // using Z value is fine, ROS takes depth image as distance along
        //     // optical axis, not distance to object
        //     // if ((pointIn[pointIndex].depthConfidence > config_.confidence_cutoff) &&
        //     //     pointIn[pointIndex].z > config_.min_depth)
        //     // {
        //     //     depthInMm = pointIn[pointIndex].z * 1000;
        //     // }

        //     if (m_flipImage)
        //     {
        //         memcpy(&m_depthImageMsg.data[(numPoints - pointIndex - 1) * 2], &depthInMm, 2);
        //     }
        //     else
        //     {
        //         memcpy(&m_depthImageMsg.data[pointIndex * 2], &depthInMm, 2);
        //     }
        // }

        if (m_publishPointCloud)
        {
            float sensor_x, sensor_y, sensor_z;

            if (m_flipImage)
            {
                sensor_x = -pointIn[pointIndex].x;
                sensor_y = -pointIn[pointIndex].y;
            }
            else
            {
                sensor_x = pointIn[pointIndex].x;
                sensor_y = pointIn[pointIndex].y;
            }

            sensor_z = pointIn[pointIndex].z;

            memcpy(&m_pointCloudMsg.data[pointIndex * m_pointCloudMsg.point_step +
                   m_pointCloudMsg.fields[0].offset], &sensor_x, sizeof(float));

            memcpy(&m_pointCloudMsg.data[pointIndex * m_pointCloudMsg.point_step +
                   m_pointCloudMsg.fields[1].offset], &sensor_y, sizeof(float));

            memcpy(&m_pointCloudMsg.data[pointIndex * m_pointCloudMsg.point_step +
                   m_pointCloudMsg.fields[2].offset], &sensor_z, sizeof(float));

            memcpy(&m_pointCloudMsg.data[pointIndex * m_pointCloudMsg.point_step +
                   m_pointCloudMsg.fields[3].offset],
                   &pointIn[pointIndex].noise, sizeof(float));

            memcpy(&m_pointCloudMsg.data[pointIndex * m_pointCloudMsg.point_step +
                   m_pointCloudMsg.fields[4].offset],
                   &pointIn[pointIndex].depthConfidence, sizeof(uint32_t));

            memcpy(&m_pointCloudMsg.data[pointIndex * m_pointCloudMsg.point_step +
                   m_pointCloudMsg.fields[5].offset],
                   &pointIn[pointIndex].grayValue, sizeof(uint16_t));
        }

        if (m_publishLaserScan)
        {
            size_t startOffset = TofImageWidth * LaserScanLine;

            if ((pointIndex >= startOffset) && (pointIndex < startOffset + TofImageWidth))
            {
                float range = sqrt((pointIn[pointIndex].x * pointIn[pointIndex].x) +
                                   (pointIn[pointIndex].z * pointIn[pointIndex].z));

                if(range > m_laserScanMsg.range_min)
                {
                    m_laserScanMsg.ranges[rowOffset] = range;
                }

                rowOffset--;
            }
        }

        if (m_publishIRImage)
        {
            if(pointIn[pointIndex].grayValue > 255)
            {
                m_irImageMsg.data[pointIndex] = 255;
            }
            else
            {
                m_irImageMsg.data[pointIndex] = pointIn[pointIndex].grayValue;
            }
        }
    }

    if (m_publishIRImage)
    {
        m_rosIRImagePublisher.publish(m_irImageMsg, m_cameraInfoMsg);
    }

    ///<@todo Fix depthConfidence logic
    // if (publishDepthImage)
    // {
    //     m_rosDepthImagePublisher.publish(m_depthImageMsg, m_cameraInfoMsg);
    // }

    if (m_publishPointCloud)
    {
        m_rosPointCloudPublisher.publish(m_pointCloudMsg);
    }

    if (m_publishLaserScan)
    {
        m_rosLaserScanPublisher.publish(m_laserScanMsg);
    }
}

// -----------------------------------------------------------------------------------------------------------------------------
// Point cloud publish function ///< @todo Needs to be fixed
// -----------------------------------------------------------------------------------------------------------------------------
void PerCameraMgr::PublishPointCloud(const void* pData)
{
    ///<@todo This is done by obtaining depth data for now. Enable point-cloud-only path
    // struct SparsePointCloud
    // {
    //     int64_t                 timestamp;     //!< timestamp for the frame
    //     StreamId                streamId;      //!< stream which produced the data
    //     uint32_t                numPoints;     //!< the number of valid points
    //     royale::Vector<float>   xyzcPoints;    //!< XYZ and confidence for every valid point
    // };
    // fwrite((uint8_t*)&data->timestamp, sizeof (data->timestamp), 1, fp);
    // fwrite((uint8_t*)&data->streamId, sizeof (data->streamId), 1, fp);
    // fwrite((uint8_t*)&data->numPoints, sizeof (data->numPoints), 1, fp);
    // fwrite((uint8_t*)&data->xyzcPoints[0], data->xyzcPoints.size() * sizeof(float), 1, fp);
    // const royale::SparsePointCloud* pSparsePointCloud = static_cast<const royale::SparsePointCloud *> (pData);

    // // fwrite((uint8_t *)(data->cdData.data()), data->cdData.size() * sizeof(uint16_t), 1, fp);
    // m_pointCloudMsg.header.stamp.fromNSec(pSparsePointCloud->timestamp);

    // m_pointCloudMsg.width     = pSparsePointCloud->numPoints;
    // m_pointCloudMsg.height    = 1;
    // m_pointCloudMsg.row_step  = pSparsePointCloud->numPoints;

    // // The total size of the channels of the point cloud for a single point are:
    // // sizeof(float) * 3 = x, y, and z
    // // sizeof(float)     = noise
    // // sizeof(uint32_t)  = depthConfidence (only the low byte is defined, using uint32_t for alignment)
    // uint32_t totalBytes = m_pointCloudMsg.height * m_pointCloudMsg.point_step;
    // m_pointCloudMsg.data.resize(totalBytes);

    // // 14 bytes for the width, height, streamid, and timestamp
    // memcpy(&m_pointCloudMsg.data[0], (uint8_t *)(pSparsePointCloud->cdData.data()), totalBytes);
    // // ^ Would prefer to use the pSparsePointCloud->data.data(), but the royale::Vector
    // // appears to be ill packed (data.size() does not return a consistent number)

    // m_pointCloudMsg.is_bigendian = false;

    // static int publishedFrame = 0;
    // printf("\nPublishing Point Cloud %d: W:%d H:%d S:%d",
    //           publishedFrame++, m_pointCloudMsg.width, m_pointCloudMsg.height, m_pointCloudMsg.step);
    // m_rosPointCloudPublisher.publish(m_pointCloudMsg);
}

// -----------------------------------------------------------------------------------------------------------------------------
// Load the TOF sensor lens parameters from "irs10x0c_lens.cal"
// -----------------------------------------------------------------------------------------------------------------------------
bool PerCameraMgr::LoadLensParamsFromFile()
{
    const char* LensParamsFilePath = "/data/misc/camera/irs10x0c_lens.cal";
    const char* RequiredVersion    = "VERSION:1.0:VERSION";

    std::vector<float> v_float;
    std::ifstream ifs;
    std::string line;
    int  maxTries = 10;
    bool status   = false;

    while (maxTries-- > 0)
    {
        ifs.open(LensParamsFilePath, std::ifstream::in);

        if( ifs.is_open() )
        {
            maxTries = 0;
        }
        else
        {
            usleep(1000000);
        }
    }

    if (!ifs.is_open())
    {
        ROS_INFO("Error: cannot open lens parameters file %s.", LensParamsFilePath );
    }
    else
    {
        status = std::getline(ifs, line);

        if (!status || strncmp(line.c_str(), RequiredVersion, strlen(RequiredVersion)) != 0)
        {
            ROS_INFO("Error: the first line of %s must be %s", LensParamsFilePath, RequiredVersion);
        }
        else
        {
            while(std::getline(ifs, line))
            {
                if(line.length() > 0 && line[0] != '#')
                {
                    float num;
                    std::stringstream linestream(line);

                    while (linestream >> num)
                    {
                        v_float.push_back(num);
                    }
                }
            }
        }

        ifs.close();
    }

    status = (v_float.size() >= 8);

    if(status)
    {
        ROS_INFO("Loading lens parameters from %s.", LensParamsFilePath);

        m_lensParameters.principalPoint       = royale::Pair<float, float>(v_float[0], v_float[1]);
        m_lensParameters.focalLength          = royale::Pair<float, float>(v_float[2], v_float[3]);
        m_lensParameters.distortionTangential = royale::Pair<float, float>(v_float[4], v_float[5]);

        for (unsigned int i = 6; i < v_float.size(); i++)
        {
            m_lensParameters.distortionRadial.push_back(v_float[i]);
        }
    }

    ROS_INFO_STREAM(
        std::endl <<
        "cx/cy     " << m_lensParameters.principalPoint.first << " " << m_lensParameters.principalPoint.second << std::endl
        << "fx/fy     " << m_lensParameters.focalLength.first << " " << m_lensParameters.focalLength.second << std::endl
        << "tan coeff "
        << (m_lensParameters.distortionTangential.first)
        << " " << (m_lensParameters.distortionTangential.second) << std::endl
        << "rad coeff "
        << m_lensParameters.distortionRadial.at (0)
        << " " << m_lensParameters.distortionRadial.at (1)
        << " " << m_lensParameters.distortionRadial.at (2) << std::endl);

     return status;
}
