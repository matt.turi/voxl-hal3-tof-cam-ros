/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef HELLO_HAL3_CAMERA
#define HELLO_HAL3_CAMERA

#include <camera/CameraMetadata.h>
#include <hardware/camera3.h>
#include <list>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>
#include <image_transport/image_transport.h>
#include <image_transport/camera_publisher.h>
#include "LensParameters.hpp"
#include "TOFInterface.h"

// Forward Declaration
class BufferManager;
class PerCameraMgr;

#define TOF_RAW_FRAME_WIDTH  224
#define TOF_RAW_FRAME_HEIGHT 1557

// -----------------------------------------------------------------------------------------------------------------------------
// Thread Data for camera request and result thread
// -----------------------------------------------------------------------------------------------------------------------------
typedef struct ThreadData
{
    pthread_t         thread;                   ///< Thread handle
    pthread_mutex_t   mutex;                    ///< Mutex for list access
    pthread_cond_t    cond;                     ///< Condition variable for wake up
    std::list<void*>  msgQueue;                 ///< Message queue
    PerCameraMgr*     pCameraMgr;               ///< Pointer to the per camera mgr class
    camera3_device_t* pDevice;                  ///< Camera device that the thread is communicating with
    void*             pPrivate;                 ///< Any private information if need be
    volatile bool     stop;                     ///< Indication for the thread to terminate
    volatile int      lastResultFrameNumber;    ///< Last frame the capture result thread should wait for before terminating
} ThreadData;

// -----------------------------------------------------------------------------------------------------------------------------
// Different camera modes
// -----------------------------------------------------------------------------------------------------------------------------
enum CameraMode
{
    CameraModePreview = 0,      ///< Preview only mode
    CameraModeVideo   = 1,      ///< Preview + Video mode
};

// -----------------------------------------------------------------------------------------------------------------------------
// Supported preview formats
// -----------------------------------------------------------------------------------------------------------------------------
enum PreviewFormat
{
    PreviewFormatNV21 = 0,      ///< NV21
    PreviewFormatRAW8 = 1,      ///< RAW8 (If the camera doesnt support RAW8, RAW10 will be requested and converted to RAW8)
    PreviewFormatBLOB = 2,      ///< BLOB (TOF camera uses this for preview format)
};

// -----------------------------------------------------------------------------------------------------------------------------
// Supported stream types
// -----------------------------------------------------------------------------------------------------------------------------
enum StreamType
{
    StreamTypePreview = 0,      ///< Preview stream
    StreamTypeVideo,            ///< Video stream
    StreamTypeMax,
};

// -----------------------------------------------------------------------------------------------------------------------------
// Supported stream types
// -----------------------------------------------------------------------------------------------------------------------------
typedef enum
{
    // RAW only mode for devices that will simultaneously use more than two cameras.
    // This mode has following limitations: Back end 3A, Face Detect or any additional functionality depending on image/sensor
    // statistics and YUV streams will be disabled

    QCAMERA3_VENDOR_STREAM_CONFIGURATION_RAW_ONLY_MODE = 0x8000,
} QCamera3VendorStreamConfiguration;

//------------------------------------------------------------------------------------------------------------------------------
// Everything needed to handle a single camera
//------------------------------------------------------------------------------------------------------------------------------
class PerCameraMgr : public tof::IRoyaleDataListener
{
public:
    PerCameraMgr(ros::NodeHandle rosNodeHandle);
    ~PerCameraMgr() { }

    // Return the number of preview frames to dump to files
    int GetDumpPreviewFrames() const
    {
        return m_dumpPreviewFrames;
    }
    // Return the TOF interface object
    void* GetTOFInterface()
    {
        return m_pTOFInterface;
    }
    // Perform any one time initialization
    int Initialize(const camera_module_t* pCameraModule,
                   const camera_info*     pCameraInfo,
                   int                    cameraid,
                   int                    width,
                   int                    height,
                   PreviewFormat          format,
                   CameraMode             mode,
                   int                    tofdatatype,
                   int                    tofnumframes,
                   const char*            pVideoFilename,
                   int                    dumpPreviewFrames);
    // Start the camera so that it starts streaming frames
    int  Start();
    // Stop the camera and stop sending any more requests to the camera module
    void Stop();
    // Send one capture request to the camera module
    int  ProcessOneCaptureRequest(int frameNumber);
    // Process one capture result sent by the camera module
    void ProcessOneCaptureResult(const camera3_capture_result* pHalResult);
    // The request thread calls this function to indicate it sent the last request to the camera module and will not send any
    // more requests
    void StoppedSendingRequest(int framenumber);
    // Callback function for the TOF bridge to provide the post processed TOF data
    bool RoyaleDataDone(const void*             pData,
                        uint32_t                size,
                        int64_t                 timestamp,
                        tof::RoyaleListenerType dataType);

private:
    // Is the camera running in preview + video mode
    bool IsVideoMode() const
    {
        return (m_cameraMode == CameraModeVideo);
    }
    // Return if this resolution corresponds to a TOF camera
    void DetermineTOFCamera(int width, int height)
    {
        if ((width == TOF_RAW_FRAME_WIDTH) && (height == TOF_RAW_FRAME_HEIGHT))
        {
            m_isTOF = true;
        }
        else
        {
            m_isTOF = false;
        }
    }
    // Check for TOF camera
    bool IsTOFCamera()
    {
        return m_isTOF;
    }

    bool LoadLensParamsFromFile();
    // Camera module calls this function to pass on the capture frame result
    static void CameraModuleCaptureResult(const camera3_callback_ops *cb,const camera3_capture_result *hal_result);
    // Camera module calls this function to notify us of any messages
    static void CameraModuleNotify(const struct camera3_callback_ops *cb, const camera3_notify_msg_t *msg);
    // Check the static camera characteristics for a matching (w, h, format) combination
    bool        IsStreamConfigSupported(int width, int height, int format);
    // Call the cameram module and pass it the stream configuration
    int         ConfigureStreams();
    // Allocate the stream buffers using the BufferManager
    int         AllocateStreamBuffers();
    // Call the camera module to get the default camera settings
    void        ConstructDefaultRequestSettings();
    // One time initialization of ROS messages
    void        InitializeRosMessages();
    // One time initialization of camera info messages
    void        InitializeCameraInfoMessage(int imageHeight, int imageWidth, std::string frame_id);
    // One time initialization of depth image messages
    void        InitializeDepthMessage(int imageHeight, int imageWidth, std::string frame_id);
    // One time initialization of IR image messages
    void        InitializeIrImageMessage(int imageHeight, int imageWidth, std::string frame_id);
    // One time initialization of point cloud messages
    void        InitializePointCloudMessage(int imageHeight, int imageWidth, std::string frame_id);
    // One time initialization of laser scan messages
    void        InitializeLaserScanMessage(std::string frame_id);
    // Publish IR image using ROS
    void        PublishIRImage(const void* pData);
    // Publish Depth image using ROS
    void        PublishDepthImage(const void* pData);
    // Publish Point cloud using ROS
    void        PublishPointCloud(const void* pData);
    // Publish Depth data using ROS
    void        PublishDepthData(const void* pData, int64_t timestamp);

    static const uint32_t MaxPreviewBuffers = 16;
    static const uint32_t MaxVideoBuffers   = 16;
    static const uint8_t  FrameRate         = 30;

    // camera3_callback_ops is returned to us in every result callback. We piggy back any private information we may need at
    // the time of processing the frame result. When we register the callbacks with the camera module, we register the starting
    // address of this structure (which is camera3_callbacks_ops) but followed by our private information. When we receive a
    // pointer to this structure at the time of capture result, we typecast the incoming pointer to this structure type pointer
    // and access our private information
    struct Camera3Callbacks
    {
        camera3_callback_ops cameraCallbacks;
        void* pPrivate;
    };

    static const int ImageQueueSize      = 10;
    static const int PointCloudQueueSize = 10;
    static const int LaseScanQueueSize   = 10;
    static const int PointCloudChannels  = 6;
    ///< @todo Remove these hardcoded values
    static const int TofImageWidth      = 224;
    static const int TofImageHeight     = 172;
    static const int LaserScanLine      = TofImageHeight / 2;

    int32_t                          m_cameraId;                        ///< Camera id
    CameraMode                       m_cameraMode;                      ///< Camera mode
    int32_t                          m_previewFormat;                   ///< Preview format
    Camera3Callbacks                 m_cameraCallbacks;                 ///< Camera callbacks
    const camera_module_t*           m_pCameraModule;                   ///< Camera module
    const camera_info*               m_pCameraInfo;                     ///< Camera info
    int                              m_width;                           ///< Width
    int                              m_height;                          ///< Height
    int                              m_tofdatatype;                     ///< TOF datatype
    int                              m_tofnumframes;                    ///< Number of TOF frames to dump
    const char*                      m_pVideoFilename;                  ///< Video filename
    FILE*                            m_pVideoFilehandle;                ///< Video file handle
    int                              m_dumpPreviewFrames;               ///< Number of preview frames to dump
    int64_t                          m_exposureSetting;                 ///< Exposure setting
    int32_t                          m_gainSetting;                     ///< Gain setting
    camera3_stream_t                 m_streams[StreamTypeMax];          ///< Streams to be used for the camera request
    camera3_device_t*                m_pDevice;                         ///< HAL3 device
    android::CameraMetadata          m_requestMetadata;                 ///< Per request metadata
    BufferManager*                   m_pBufferManager[StreamTypeMax];   ///< Buffer manager per stream
    ThreadData                       m_requestThread;                   ///< Request thread private data
    ThreadData                       m_resultThread;                    ///< Result Thread private data
    bool                             m_isTOF;                           ///< Is this a TOF camera
    void*                            m_pTOFInterface;                   ///< TOF interface to process the TOF camera raw data
    royale::LensParameters           m_lensParameters;                  ///< Lens parameters
    int                              m_laserScanLine;                   ///< Laser scan line
    int                              m_scanWidthDegrees;                ///< Scan witdh degrees

    // ROS specific members
    sensor_msgs::CameraInfo          m_cameraInfoMsg;                   ///< Camera Info message
    sensor_msgs::Image               m_depthImageMsg;                   ///< Depth Image message
    sensor_msgs::Image               m_irImageMsg;                      ///< IR Image    message
    sensor_msgs::PointCloud2         m_pointCloudMsg;                   ///< Point cloud message
    sensor_msgs::LaserScan           m_laserScanMsg;                    ///< Laser scan  message
    ros::NodeHandle                  m_rosNodeHandle;                   ///< ROS node handle
    ros::Publisher                   m_rosPointCloudPublisher;          ///< ROS point cloud publisher
    ros::Publisher                   m_rosLaserScanPublisher;           ///< ROS laser scan publisher
    image_transport::ImageTransport  m_rosImageTransport;               ///< ROS Image transport
    image_transport::CameraPublisher m_rosDepthImagePublisher;          ///< Depth Image publisher
    image_transport::CameraPublisher m_rosIRImagePublisher;             ///< IR Image publisher
    bool                             m_flipImage;                       ///< Flip image
    bool                             m_publishDepthImage;               ///< Publish depth image
    bool                             m_publishIRImage;                  ///< Publish IR image
    bool                             m_publishLaserScan;                ///< Laser scan
    bool                             m_publishPointCloud;               ///< Point cloud
};

//------------------------------------------------------------------------------------------------------------------------------
// Main interface class to manage all the cameras
//------------------------------------------------------------------------------------------------------------------------------
class CameraHAL3
{
public:
    CameraHAL3();
    ~CameraHAL3() { }
    // Perform any one time initialization
    int Initialize();
    // Start a camera with the requested parameters
    int Start(int             cameraid,
              int             width,
              int             height,
              PreviewFormat   format,
              CameraMode      mode,
              int             tofdatatype,
              int             tofnumframes,
              const char*     pVideoFilename,
              int             dumpPreviewFrames,
              ros::NodeHandle rosNodeHandle);
    // Stop the opened camera and close it
    void Stop();

    // Automatically determine ID of TOF camera based on output stream dimensions
    int FindTofCamera();

private:
    // Callback to indicate device status change
    static void CameraDeviceStatusChange(const struct camera_module_callbacks* callbacks, int camera_id, int new_status)
    {
    }
    // Callback to indicate torch mode status change
    static void TorchModeStatusChange(const struct camera_module_callbacks* callbacks, const char* camera_id, int new_status)
    {
    }

    // Load the camera module to start communicating with it
    int LoadCameraModule();

    void PrintCameraCapabilities(camera_info * pCameraInfo);

    static const uint32_t MaxCameras = 4;

    camera_module_t*        m_pCameraModule;                ///< Camera module
    camera_module_callbacks m_moduleCallbacks;              ///< Camera module callbacks
    PerCameraMgr*           m_pPerCameraMgr[MaxCameras];    ///< Each instance manages one camera
    int32_t                 m_numCameras;                   ///< Number of cameras detected
    camera_info             m_cameraInfo[MaxCameras];       ///< Per camera info
};

#endif // HELLO_HAL3_CAMERA
